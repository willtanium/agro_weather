package ug.aidev.services;

import android.content.Context;
import android.util.Base64;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ug.aidev.agroweather.R;
import ug.aidev.models.Field;
import ug.aidev.models.ForeCast;

/**
 * Created by william on 6/23/16.
 */
public class WeatherHandler {
    private WeatherService weatherService;
    private Context context;
    private String auth;
    private RestAdapter restAdapter;
    private OnForecastReceived forecastReceived;
    public WeatherHandler(Context context){
        this.context = context;
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(context.getResources().getString(R.string.end_point));
        String consumerKey = context.getResources().getString(R.string.agro_key);
        String clientSecret = context.getResources().getString(R.string.agro_client_secret);
        restAdapter = builder.build();
        weatherService = restAdapter.create(WeatherService.class);
        auth ="Basic "+ Base64.encodeToString((consumerKey + ":" + clientSecret).getBytes(),
                Base64.NO_WRAP);
    }


    void getForecast(Field field, OnForecastReceived forecastReceived){
        this.forecastReceived = forecastReceived;
        weatherService.getForecasts(auth, field.getFieldId(), new ForeCastCallback());
    }
    void getForecastByDate(Field field, String date,OnForecastReceived forecastReceived){
        this.forecastReceived = forecastReceived;
        weatherService.getForeCastsByDate(auth, field.getFieldId(), date, new ForeCastCallback());
    }

    void getForecastsFrom(Field field, String startDate, String endDate, OnForecastReceived forecastReceived){
        this.forecastReceived = forecastReceived;
        weatherService.getForeCastsFrom(auth, startDate, endDate, new ForeCastCallback());
    }

    private class ForeCastCallback implements Callback<ForeCast> {
        @Override
        public void success(ForeCast foreCast, Response response) {
            if(forecastReceived != null){
                forecastReceived.forecastReceived(foreCast);
            }
        }

        @Override
        public void failure(RetrofitError error) {
                if(forecastReceived != null){
                    forecastReceived.networkError(error.getLocalizedMessage());
                }
        }
    }
}
