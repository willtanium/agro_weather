package ug.aidev.services;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import ug.aidev.models.ForeCast;

/**
 * Created by william on 6/23/16.
 */
public interface WeatherService {

    @GET("/v2/weather/fields/{fieldId}/forecasts")
    void getForecasts(@Header("Authorization") String authorization,@Path("fieldId") String fieldId, Callback<ForeCast> fcCB);
    @GET("/v2/weather/fields/{fieldId}/forecasts/{singleDate}")
    void getForeCastsByDate(@Header("Authorization") String authorization,
                            @Path("fieldId") String fieldId, @Path("singleDate")
    String singleDate, Callback<ForeCast> fcCB);
    @GET("/v2/weather/fields/{fieldId}/forecasts/{startDate},{endDate}")
    void getForeCastsFrom(@Header("Authorization") String authorization,
                          @Path("startDate") String startDate, @Path("endDate") String endDate,
                          Callback<ForeCast> fcCB);
}
