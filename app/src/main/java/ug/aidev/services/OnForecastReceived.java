package ug.aidev.services;

import ug.aidev.models.ForeCast;

/**
 * Created by william on 6/23/16.
 */
public interface OnForecastReceived {
    void forecastReceived(ForeCast foreCast);

    void networkError(String localizedMessage);
}
