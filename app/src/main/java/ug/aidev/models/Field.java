package ug.aidev.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 6/23/16.
 */
public class Field implements Serializable{
    public String getFarmId() {
        return farmId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public Integer getAcres() {
        return acres;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Point getPoint() {
        return point;
    }

    public Link getLink() {
        return link;
    }

    @SerializedName("farmId")
    private String farmId;
    @SerializedName("id")
    private String fieldId;
    @SerializedName("acres")
    private Integer acres;
    @SerializedName("name")
    private String fieldName;
    @SerializedName("centerPoint")
    private Point point;
    @SerializedName("_links")
    private Link link;
}
